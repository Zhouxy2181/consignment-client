module consignment-client

go 1.13

require (
	github.com/golang/protobuf v1.3.3
	golang.org/x/net v0.0.0-20200222125558-5a598a2470a0
	google.golang.org/grpc v1.27.1
)
