package main

import (
	pb "consignment-client/proto/consignment"
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/grpc"
	"io/ioutil"
	"log"
)

const (
	address         = "localhost:50051"
	defaultFileName = "./consignment.json"
)

func parseFile(file string) (*pb.Consignment, error) {
	var consignment *pb.Consignment
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &consignment)
	if err != nil {
		return nil, err
	}

	return consignment, nil
}

func main() {
	cnn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatal("did not connect", err)
	}
	defer cnn.Close()
	client := pb.NewShippingServiceClient(cnn)

	consignment, err := parseFile(defaultFileName)
	if err != nil {
		log.Fatal("get data failed", err)
	}

	rsp, err := client.CreateConsignment(context.Background(), consignment)
	if err != nil {
		log.Fatal("get rsp data err", err)
	}
	fmt.Println(rsp.Created)

	rsps, err := client.GetConsignments(context.Background(), &pb.GetRequest{})
	if err != nil {
		log.Fatal("get rsps data fail", err)
	}
	for _, v := range rsps.Consignments {
		fmt.Println(v)
	}
}
